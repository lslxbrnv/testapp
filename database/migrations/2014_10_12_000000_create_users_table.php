
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('address')->nullable();
            $table->string('contact_number')->nullable();
            $table->tinyInteger('role')->default(1);
            $table->boolean('verified')->default(false);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes(); 
        });

        User::create([
            'name' => 'Mr. Green',
            'email' => 'admin@gmail.com',
            'address' => 'Bacolod City',
            'contact_number' => '4341111',
            'password' => bcrypt('p@ssw0rd'),
            'verified' => 1
        ]
        );

         User::create([
            'name' => 'Customer Test',
            'email' => 'customer@gmail.com',
            'role' => 2,
            'address' => 'Bacolod City',
            'contact_number' => '4341000',
            'password' => bcrypt('p@ssw0rd'),
            'verified' => 1
        ]
        );
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
