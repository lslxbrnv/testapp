<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashRegisterItems extends Migration
{
    public function up()
    {
        Schema::create('cash_register_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cash_register_id')->unsigned();
            $table->integer('item_id')->unsigned();
            
            $table->decimal('price', 5, 2);
            $table->integer('quantity');
            
            $table->index(['cash_register_id', 'item_id']);

            $table->foreign('cash_register_id')->references('id')->on('cash_register');
            $table->foreign('item_id')->references('id')->on('item');
        });
    }

    public function down()
    {
        Schema::drop('cash_register_items');
    }
}
