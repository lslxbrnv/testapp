<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/a')
->middleware('auth:api')
->group(function(){
	Route::prefix('/')
	->group(function(){
		Route::get('user', function() { return Auth::user(); });
		#Route::post('device', 'MobileUserDevicesController@register');
		#Route::get('schedule/{id}', 'ShiftSwapInformationController@api_details_json');
	});

	/*Route::prefix('shift-swap')
	->group(function(){
		$this->get('search', 'ShiftSwapInformationController@search_json'); 
		$this->get('type', 'ShiftSwapController@swap_type'); 
		$this->get('week-ending-and-my-shift-date/{id}', 'ShiftSwapInformationController@ss_we_and_my_sd');
		$this->post('store', 'ShiftSwapController@store');
		$this->post('confirm', 'ShiftSwapController@request_confirmation');
	});*/

	/*Route::prefix('firebase')
    ->group(function(){
    	$this->post('send', 'CurlFirebaseCloudMessagingController@send_to_device');
    });*/
});