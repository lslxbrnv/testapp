@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-bars"></i>&nbsp;Transaction No. {{ str_pad($transactionid, 10, '0', STR_PAD_LEFT) }}</h3>
        </div>
        <div class="column is-3 ">
            <a target="_blank" href="{{ route('official_receipt', $transactionid) }}" class="button is-info is-pulled-right">
                <i class="fa fa-print"></i> &nbsp;Print
            </a>
        </div>
    </div>
    <table class="table is-fullwidth">
        <thead>
            <tr>
                <th width="35%">Name</th>
                <th width="10%">Contact No.</th>
                <th width="55%">Address</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $transaction_user->user->name }}</td>
                <td>{{ $transaction_user->user->contact_number }}</td>
                <td>{{ $transaction_user_details->address }}</td>
            </tr>
            <tr>
                <td colspan="1"><strong> Has change for : </strong></td>
                <td colspan="4">P {{ $transaction_user->cash_received }}</td>
            </tr>
        </tbody>
    </table>
     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr> 
                <th width="30%">Item</th>
                <th width="40%">Quantity</th>
                <th width="20%">Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transaction_user_items as $transaction_user_item)
            <tr>
                <td>{{ $transaction_user_item->item->first()->name }}</td>
                <td>{{ $transaction_user_item->quantity }}</td>
                <td>{{ $transaction_user_item->price }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    @if ($transaction_user->status == 1)
    <div class="field has-addons is-pulled-right">
        <div class="control">
            <a href="{{ route('accept_order', $transactionid) }}" class="button is-success ">Accept Order</a>  
        </div> &nbsp;
        <div class="control">
            <a href="{{ route('invalid_order', $transactionid) }}" class="button is-danger ">Invalid Order</a>  
        </div>
    </div>
    @endif
@endsection
