@extends('layouts.empty')

@section('content')
@parent
<style type="text/css">
body{
	font-size: 12px!important;
	overflow: hidden;
	width: 100%;
		margin: 0;
	padding: 0;
}
table th,
table td,
table tr{
	padding: 0!important;
	margin: 0!important;
	height: 0px!important;
}
table td{
	border: none!important;
}
table th{
	border-top: 1px solid rgba(0, 0, 0, 0.12);
	border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}

</style>
<table class="table is-fullwidth is bordered" width="100%">
	<thead>
		<tr>
			<td style="text-align: center;" colspan="4"><strong style="font-size:15px;">Online Store Official Receipt</strong></td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="4"><strong>{{ $supplier->address }}</strong></td>
		</tr>
		<tr>
			<td style="text-align: left;" colspan="1">O.R. No.</td>
			<td style="text-align: right;" colspan="3">{{ str_pad($transactionid, 10, '0', STR_PAD_LEFT) }}</td>
		</tr>
		<tr>
			<td style="text-align: left;" colspan="1">Transaction Date</td>
			<td style="text-align: right;" colspan="3">{{ $transaction->created_at }}</td>
		</tr>
		<tr>
			<td style="text-align: left;" colspan="1">Customer</td>
			<td style="text-align: right;" colspan="3">{{ $transaction->user->name }}</td>
		</tr>
		<tr>
			<td style="text-align: left;" colspan="1">Contact Number</td>
			<td style="text-align: right;" colspan="3">{{ $transaction_details->contact_number }}</td>
		</tr>
		<tr>
			<th style="text-align: left;" colspan="1">Item Name</th>
			<th style="text-align: right;" colspan="1">Quantity</th>
			<th style="text-align: right;" colspan="1">Price</th>
			<th style="text-align: right;" colspan="1">Total</th>
		</tr>
	</thead>
	<tbody>
		@php 
			$total=0; 
			$grandtotal=0;
		@endphp
		@foreach($transaction_items as $transaction_item)
		<tr>
			<td colspan="1" style="text-align: left;">{{ $transaction_item->item->first()->name }}</td>
			<td colspan="1" style="text-align: right;">{{ $transaction_item->quantity }}</td>
			<td colspan="1" style="text-align: right;">P {{ $transaction_item->price }}</td>
			<td colspan="1" style="text-align: right;">P {{ number_format($total =  $transaction_item->price * $transaction_item->quantity,2) }}</td>
		</tr>
		@php  
			$grandtotal = $total+$grandtotal;
		@endphp
		@endforeach
		<tr>
			<td id="grandtotal" colspan="3" style="text-align: right;">Total Bill :  </td>

			<td id="grandtotal" colspan="1" style="text-align: right;"><strong>P {{ number_format($grandtotal,2) }}</strong></td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
$(document).ready(function(){
	setTimeout(function(){ window.print(); 	}, 1000);
	setTimeout(function(){ close(); 		}, 2000);
});
</script>
@endsection