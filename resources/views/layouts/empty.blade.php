<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KMart Online Grocery Official Receipt</title>
    <style>
        body{
            background-color:#f3f3f3;
        }
    </style>
    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <script src="{{ asset('js/all.js') }}"></script>
    <script src="{{ asset('js/jquery.js') }}"></script>
</head>
<body style="width: 100%">
        @yield('content')
</body>
</html>
