@if(Session::has('success'))
    <div class="notification is-success">{{ Session::get('success') }}</div>
@elseif(count($errors) > 0)
    <div class="notification is-warning">
        <ul class="no-bullet">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif(Session::has('warning'))
    <div class="notification is-warning">{{ Session::get('warning') }}</div>
@endif
