@extends('layouts.app')

@section('content')
    <div class="grid-container">
        <div class="item1"></div>
        <div class="item1">
            <div class="login-border">
                <h1 class="login-title">K Mart</h1>
                <h4 class="login-title-sub">Online Grocery</h4>
                <form class="login-form" method="POST" action="{{ route('verify_user_forgot_password') }}">
                    {{ csrf_field() }}
            
                    <input class="input" id="email" type="email" name="email" placeholder="Enter E-Mail" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <p class="login-warning-message">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <button type="submit" class="button-success">Register</button>
                </form>
                <p>Already have an account? <a href="{{ route('login') }}">Login</a></p>
            </div>
        </div>
        <div class="item1"></div>
    </div>
@endsection
