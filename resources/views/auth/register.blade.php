@extends('layouts.app')

@section('content')
    <div class="grid-container">
        <div class="item1"></div>
        <div class="item1">
            <div class="login-border">
                <h1 class="login-title">K Mart</h1>
                <h4 class="login-title-sub">Online Grocery</h4>
                <form class="login-form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <input class="input" id="name" type="text" name="name" placeholder="Name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <p class="login-warning-message">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                    <input class="input" id="email" type="email" name="email" placeholder="E-Mail" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <p class="login-warning-message">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                    <input class="input" id="password" type="password" name="password" placeholder="Password" required>
                    <input class="input" id="password_confirmation" type="password" name="password_confirmation" placeholder="Password Confirmation" required>
                    @if ($errors->has('password'))
                        <p class="warning-message">
                            {{ $errors->first('password') }}
                        </p>
                    @endif
                    <input class="input" id="address" type="text" name="address" placeholder="Address" value="{{ old('address') }}" required>
                    @if ($errors->has('address'))
                        <p class="login-warning-message">
                            {{ $errors->first('address') }}
                        </p>
                    @endif
                    <input class="input" id="contact_number" type="text" name="contact_number" placeholder="Contact Number" value="{{ old('contact_number') }}" required>
                    @if ($errors->has('contact_number'))
                        <p class="login-warning-message">
                            {{ $errors->first('contact_number') }}
                        </p>
                    @endif
                    <br><br>
                    <button type="submit" class="button-success">Register</button>
                </form>
                <p>Already have an account? <a href="{{ route('login') }}">Login</a></p>
            </div>
        </div>
        <div class="item1"></div>
    </div>
@endsection
