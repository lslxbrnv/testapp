@extends('layouts.cust')

@section('content')
    <br>
    <section class="hero is-success">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    Success! <i class="fa fa-check-circle"></i>
                </h1>
                <h2 class="subtitle">
                    Your order has been sent. Transaction No : {{ str_pad($transactionid, 10, '0', STR_PAD_LEFT) }}
                </h2>
            </div>
        </div>
    </section>
   

@endsection
