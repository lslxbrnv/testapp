@extends('layouts.cust')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-eye"></i>&nbsp;Transaction No.  {{ str_pad($transactionid, 10, '0', STR_PAD_LEFT) }}</h3>
        </div>
    </div>

     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr> 
                <th width="40%">Item </th>
                <th width="30%">Price</th>
                <th width="30%">Quantity</th>
            </tr>
        </thead>
        <tbody>
           <tbody>
            @forelse ($transaction_items as $transaction_item)
                <tr>   
                    <td>{{ $transaction_item->item->first()->name }}</td>
                    <td>{{ $transaction_item->price }}</td>
                    <td>{{ $transaction_item->quantity }}</td>
                </tr>
                @empty
                <tr>   
                    <td class="has-text-centered" colspan="3">No Current transaction.</td>
                </tr>
            @endforelse
            <tr>
                <td colspan="3"><strong>Address</strong></td>
            </tr>
            <tr>
                <td colspan="3">{{ $transaction_user_details->address }}</td>
            </tr>
            <tr>
               <td colspan="3"><strong>Contact Number</strong></td>
            </tr>
            <tr>
                <td colspan="3">{{  $transaction_user_details->contact_number }}</td>
            </tr>
           </tbody>
        </tbody>
    </table>

@endsection
