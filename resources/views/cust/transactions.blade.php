@extends('layouts.cust')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-bars"></i>&nbsp;Transactions</h3>
        </div>
        <div class="column is-3">
         
        </div>
    </div>

     <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr> 
                <th width="5%"></th>
                <th width="12%">Transaction No.</th>
                <th width="40%">Transaction Date</th>
                <th width="20%">Delivery Date</th>
                <th width="20%">Status</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($transactions as $transaction)
            <tr>   
                <td class="has-text-centered">
                    <a href="{{ route('view_cust_transaction', $transaction->id) }}" title="View details">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </td>
                <td>{{ $transaction->id }}</td>
                <td>{{ $transaction->created_at }}</td>
                <td>{{ $transaction->delivery_date }}</td>
                <td>{{ $transaction->current_status }}</td>
            </tr>
            @empty
            <tr>   
                <td class="has-text-centered" colspan="5">No transactions yet.</td>
            </tr>
            @endforelse
        </tbody>
    </table>

@endsection
