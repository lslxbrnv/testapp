<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CashRegisterItems extends Model
{
    public $table = 'cash_register_items';
    public $fillable = ['cash_register_id', 'price', 'item_id', 'quantity'];
    public $timestamps = false;

    public function CashRegister()
    {
        return $this->hasMany('App\CashRegister');
    }

    public function cash_register()
    {
        return $this->belongsTo('App\CashRegister', 'cash_register_id');
    }

    public function Items()
    {
    	return $this->belongsTo('App\Items', 'item_id');
    }

   /* public function scopeCartItemsNotGroupBy($query, $id, $user_id)
    {
        return $query->whereCashRegisterId( $id )
                     ->whereHas('cash_register', function($query) use ($user_id){
                        $query->whereUserId($user_id);
                     })
                     ->orderBy('item_id', 'ASC');
    }

    public function scopeCartItemsGroupBy($query, $menu_id, $user_id)
    {
        return $query->select( 'id', 'menu_id', 'item_id', DB::raw("COUNT(item_id) AS qty") )
                     ->with('cash_register', 'Items')
                     ->whereMenuId($menu_id)
                     ->whereHas('cash_register', function($query) use ($user_id){
                        $query->whereUserId($user_id);
                     })
                     ->groupBy('item_id')->orderBy('item_id', 'ASC');
    }*/
}
