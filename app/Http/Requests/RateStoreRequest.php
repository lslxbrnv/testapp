<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RateStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => 'required|unique:daily_rate_per_hour,date', 
            'rate' => 'required|numeric'
        ];
    }

     public function attributes()
    {
        return [
            'date' => 'Date', 
            'rate' => 'Rate'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'numeric'    => ':attribute is not a number.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
