<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DriverUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(3);
        return [
            'license_number' => 'required|unique:driver,license_number,' . $id . ',id', 
            'license_type_id' => 'required', 
            'expiry_date' => 'required', 
            'agency_code' => 'required', 
            'first_name' => 'required', 
            'middle_name' => 'required', 
            'last_name' => 'required', 
            'birth_date' => 'required', 
            'gender' => 'required', 
            'nationality' => 'required', 
            'height' => 'required', 
            'weight' => 'required', 
            'email' => 'required|email', 
            'address' => 'required', 
            'city' => 'required'
        ];
    }

     public function attributes()
    {
        return [
            'license_number' => 'License Numberr', 
            'license_type_id' => 'License Type', 
            'expiry_date' => 'Expiry Date', 
            'agency_code' => 'Agency Code', 
            'first_name' => 'First Name', 
            'middle_name' => 'Middle Name', 
            'last_name' => 'Last Name', 
            'birth_date' => 'Birth Date', 
            'gender' => 'Gender', 
            'nationality' => 'Nationality', 
            'height' => 'Height', 
            'weight' => 'Weight', 
            'email' => 'Email', 
            'address' => 'Address', 
            'city' => 'City'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'email'     => ':attribute is not an email.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
