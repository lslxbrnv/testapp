<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RateUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => 'required|unique:daily_rate_per_hour,date,' . $this->rate . ',id', 
            'rate' => 'required|numeric'
        ];
    }

     public function attributes()
    {
        return [
            'date' => 'Date', 
            'rate' => 'Rate'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'numeric'    => ':attribute is not a number.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
