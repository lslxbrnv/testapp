<?php

namespace App\Http\Controllers\Auth;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/portal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     /*protected function authenticated(Request $request)
    {
        $role = Auth::user()->role;
        if($role == 1) {
            return redirect()->route('home');
        }else if($role == 2) {
            return redirect()->route('cust');
        }else{
            dd('echo');
        }
    } */

    public function authenticated(Request $request, $user)
    {
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        
        $role = Auth::user()->role;
        if($role == 1) {
            return redirect()->route('home');
        }else if($role == 2) {
            return redirect()->route('cust');
        }else{
            dd('echo');
        }
        return redirect()->intended($this->redirectPath());
    }
}
