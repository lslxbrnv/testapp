<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Auth;
use Response;

use App\Items;
use App\Category;
use App\CashRegister;
use App\CashRegisterItems;

use App\Transaction;
use App\TransactionItem;
use App\TransactionUserDetails;

use App\User;


use \Carbon\Carbon;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $ex = explode('/', $request->path());
        $id = end($ex);
		$search = '';
		if($request->has('search')){
			$search = $request->search;
            if(is_numeric($id))
            {
                $items = Items::with('category')->where('name', 'LIKE', "%$search%")->where('category_id', $id)->get();
            }else{
                $items = Items::with('category')->where('name', 'LIKE', "%$search%")->get();
            }
           
		}else{
            if(is_numeric($id))
            {
			 $items = Items::with('category')->where('category_id', $id)->get();
            }else{
             $items = Items::with('category')->get();
            }
		}
        $categories = Category::get();


        $data = compact('items', 'search', 'categories', 'id');

        return view('cust.items', $data);
    }


    public function add_to_cart(Request $request)
    {

    	$check_if_user_has_cart = CashRegister::where('user_id', Auth::user()->id);
    	if($check_if_user_has_cart->get()->count() < 1)
    	{
    		$cart = new CashRegister;
	        $cart->user_id = Auth::user()->id;
	        $cart->created_at = Carbon::now();
	        $cart->save();

	        $cart_id = $cart->id;
    	}else{
    		$cart_id = $check_if_user_has_cart->first()->id;
    		
    	}
        $price  = '$request->hidden_price'.$request->item_id;

        $cart_items = new CashRegisterItems;
        $cart_items->cash_register_id = $cart_id;
        $cart_items->item_id = $request->item_id;
        $cart_items->price = $request->price;
        $cart_items->quantity = $request->quantity;
        $cart_items->save();

        return redirect()->route('cust_items')->with('message', 'Success');
    }

    public function proceed_to_checkout()
    {
    	$check_if_user_has_cart = CashRegister::where('user_id', Auth::user()->id)->first();
    	if($check_if_user_has_cart != null)
    	{
    		$cart_items = CashRegisterItems::with(['items' => function ($q) {
                                   $q->withTrashed();
                                }])->with('items.category')
    						->where('cash_register_id', $check_if_user_has_cart->id)
    						->get();
    	}else{
            $cart_items = null;
        }

    	$data = compact('check_if_user_has_cart','cart_items');

        return view('cust.checkout', $data);
    }

    public function remove_item_from_cart($itemid)
    {
    	$item = CashRegisterItems::find($itemid);
        $item->delete();

        $cash_register_id = CashRegister::where('user_id', Auth::user()->id)->pluck('id');
        $check_if_items_empty = CashRegisterItems::where('cash_register_id', $cash_register_id)->count();


        if($check_if_items_empty < 1)
        {
            $remove_cart = CashRegister::find($cash_register_id);
            $remove_cart->delete();
        }


        $msg = "Item has been removed.";

        return redirect()->route('proceed_to_checkout')->with('success', $msg);
    }

    public function confirm_customer_details()
    {
        $user = User::whereId(Auth::user()->id)->first();
        $cash_register = CashRegister::where('user_id', Auth::user()->id)->first();
        $cash_reg_items = CashRegisterItems::where('cash_register_id', $cash_register->id)->get();

            $total_bill = 0;
        foreach($cash_reg_items as $cash_reg_item)
            {
                $total_bill += $cash_reg_item->price * $cash_reg_item->quantity;
            }

            #total bill plus delivery fee
            $total_bill = $total_bill + 180;
             $data = compact('user','total_bill');
    	return view('cust.customer-details', $data);

    }

    public function confirmed_checkout(Request $request)
    {
    	$check_if_user_has_cart = CashRegister::where('user_id', Auth::user()->id);
    	if($check_if_user_has_cart->first()->count() > 0)
    	{
            $this->validate($request, [
                'name' => 'required|unique:item,name,NULL,id,category_id,'.$request->category_id.',deleted_at,NULL|max:255',
                'address' => 'required',
                'contact_number' => 'required|numeric',
                'cash_received' => 'required|numeric',
                'delivery_date' => 'required|max:255'
            ]);
            $datetime_minimum_now = Carbon::now()->addHours(2);
            $delivery_date  = Carbon::parse($request->delivery_date);

            $cash_register = CashRegister::where('user_id', Auth::user()->id)->first();

            $cash_reg_items = CashRegisterItems::where('cash_register_id', $cash_register->id)->get();

            $total_bill = 0;

            foreach($cash_reg_items as $cash_reg_item)
            {
                $total_bill += $cash_reg_item->price * $cash_reg_item->quantity;
            }

            #total bill plus delivery fee
            $total_bill = $total_bill + 180;


            if($request->cash_received < $total_bill)
            {
                 return redirect()->route('confirm_customer_details')->withErrors(['Cash on Hand is less than the total bill.']);
            }

             if($delivery_date < $datetime_minimum_now)
            {
                 return redirect()->route('confirm_customer_details')->withErrors(['Schedule of delivery must be atleast Two(2) hours ahead from now.']);
            }

            $update_user = User::find($request->user_id);
            $update_user->name = $request->name;
            $update_user->address = $request->address;
            $update_user->updated_at = Carbon::now();
            $update_user->save();

    		$cart_items = CashRegisterItems::with(['items' => function ($q) {
                                   $q->withTrashed();
                                }])->with(['items.category' => function ($s) {
                                   $s->withTrashed();
                                }])
    						->where('cash_register_id', $check_if_user_has_cart->first()->id)
    						->get();

            #save new transaction
    		$transaction = new Transaction;
			$transaction->cash_received = $request->cash_received;
            $transaction->delivery_date = $request->delivery_date;
            $transaction->user_id = Auth::user()->id;
			$transaction->status = 1;
        	$transaction->save();

            #save to transaction items
            foreach($cart_items as $cart_item)
            {
                $transaction_item = new TransactionItem;
                $transaction_item->transaction_id = $transaction->id;
                $transaction_item->item_id = $cart_item->item_id;
                $transaction_item->price = $cart_item->price;
                $transaction_item->quantity = $cart_item->quantity;
                $transaction_item->save();
            }

            #save to transaction customer details
            $transaction_user_details = new TransactionUserDetails;
            $transaction_user_details->transaction_id = $transaction->id;
            $transaction_user_details->contact_number = $request->contact_number;
            $transaction_user_details->address = $request->address;
            $transaction_user_details->location = $request->address;
            $transaction_user_details->created_at = Carbon::now();
            $transaction_user_details->save();

            #remove items from cart
            $get_cart_id = CashRegister::where('user_id', Auth::user()->id)->pluck('id');
            $remove_cart_items = CashRegisterItems::where('cash_register_id', $get_cart_id)->delete();
            $remove_cart = CashRegister::where('id', $get_cart_id);
            $remove_cart->delete();
            #exit;

    	}else{
    		 return redirect()->route('proceed_to_checkout');
    	}



        return redirect()->route('order_success', $transaction->id)->with('message', 'Success');

    }

    public function order_success($transactionid)
    {
        $data = compact('transactionid');

        return view('cust.success', $data);
    }

    public function transactions()
    {
        $transactions = Transaction::where('user_id',Auth::user()->id)->get();
        $data = compact('transactions');
        return view('cust.transactions', $data);
    }

    public function view_transaction($transactionid)
    {
        $transaction_user_details = TransactionUserDetails::where('transaction_id', $transactionid)->first();

        $transaction_items = TransactionItem::with(['item' => function ($q) {
                                      $q->withTrashed();
                                  }])->where('transaction_id', $transactionid)->get();
     #   dd($transaction);
        $data = compact('transaction_user_details', 'transaction_items', 'transactionid');

        return view('cust.view-transactions', $data);
    }
}
