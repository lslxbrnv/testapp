<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Response;

use App\Items;
use App\User;
use App\Supplier;
use App\Category;
use App\SoftDeletes;


use App\Transaction;
use App\TransactionItem;
use App\TransactionUserDetails;

use \Carbon\Carbon;


class TransactionController extends Controller
{
    public function index()
    {
        $active_users = User::pluck('id');
    	$transactions = Transaction::whereIn('user_id', $active_users)->get();
    	$data = compact('transactions');
    	return view('transactions.index', $data);
    }

    public function view_transaction($transactionid)
    {
    	$transaction_user = Transaction::with('user')->where('id', $transactionid)->first();
    	$transaction_user_details = TransactionUserDetails::where('transaction_id', $transactionid)->first();
    	$transaction_user_items = TransactionItem::with(['item' => function ($q) {
                                      $q->withTrashed();
                                  }])->where('transaction_id', $transactionid)->get();
    	$data = compact('transactionid', 'transaction_user', 'transaction_user_details', 'transaction_user_items');
    	return view('transactions.view-transaction', $data);
    }

    public function accept_order($transactionid)
    {
    	$update_transaction = Transaction::find($transactionid);
    	$update_transaction->status = 2;
        $update_transaction->updated_at = Carbon::now();
    	$update_transaction->save();


        return redirect()->route('transactions')->with('success', 'Order has been accepted!');    
    }

    public function invalid_order($transactionid)
    {
    	$update_transaction = Transaction::find($transactionid);
    	$update_transaction->status = 3;
        $update_transaction->updated_at = Carbon::now();
    	$update_transaction->save();


        return redirect()->route('transactions')->with('success', 'Order has been invalidated!');    
    }

    public function official_receipt($transactionid)
    {
        $transaction = Transaction::with('user')->whereId($transactionid)->first();

        $transaction_items = TransactionItem::with(['item' => function ($q) {
                                      $q->withTrashed();
                                  }])->where('transaction_id', $transactionid)->get();

        $transaction_details = TransactionUserDetails::where('transaction_id', $transactionid)->first();

        $supplier = Supplier::whereId(1)->first();

        $data = compact('transactionid', 'transaction', 'transaction_items', 'transaction_details', 'supplier');

        return view('transactions.or', $data);
    }
}
