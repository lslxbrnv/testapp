<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password', 'address', 'contact_number', 'verified', 'role', 'created_at', 'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

       protected $dates = [
        'deleted_at'
    ];

    public function getAccessStatusAttribute()
    {
    	if($this->role == 1)
    	{
    		return 'ADMIN';
    	}
    	return 'CUSTOMER';
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }
}
