<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $connection = 'mysql2';

    public $table = 'cart';
    public $fillable = ['user_id', 'menu_id', 'size_id',' price_id', 'quantity', 'created_at'];  
    public $timestamps = false;
    
   /* public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }*/

    public function menu()
    {
    	return $this->belongsTo('App\Menu', 'menu_id', 'id')->withTrashed();
    }

  
}
