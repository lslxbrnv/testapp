<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CashRegister extends Model
{
    public $table = 'cash_register';
    public $fillable = [ 'user_id'];
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];
  
    public function User()
    {
    	return $this->hasMany('App\User');
    }
}
